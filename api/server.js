/*
    server
    chamillo_stats

    Created by Swano 27/02/19 12:58
*/

require('dotenv').load();
var morgan = require('morgan');
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var cors = require('cors');
var DataSet = require('./model/datas');
var PoolSchema = DataSet.PoolSchema;


var app =	express();

var port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(morgan(process.env.NODE_ENV || 'dev'));

app.use(cors({
	methods: "GET",
	maxAge: 86400
}));

// connect to mongodb
mongoose.connect("mongodb://mongodb:27017/chamilo",  function (err) {
	if (err) return console.log(err);
	console.log("Api version 0.1");

	app.get("/current_users", function (req, res) {
		PoolSchema.findOne({}).sort({utc_time: -1}).exec(function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})

	});

	app.get("/avg/hours", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$hour: "$utc_time"},  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/avg/alltime", function(req,res){
		PoolSchema.aggregate([{$group: {_id: null,  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/max/alltime", function (req, res) {
		PoolSchema.aggregate([{$group: {_id: null, max:{$max: "$online_users"}}}], function (err, max) {
			if (err || !max){
				res.status(512).send({success: false, error: err.message})
			} else {
				PoolSchema.findOne({online_users: max[0]["max"]}, function (err, all) {
					if (err || !all){
						res.status(512).send({success: false, error: err.message})
					} else {
						res.status(200).send({success: true, data: all})
					}
				})
			}
		})
	});
	app.get("/min/alltime", function (req, res) {
		PoolSchema.aggregate([{$group: {_id: null, min:{$min: "$online_users"}}}], function (err, max) {
			if (err || !max){
				res.status(512).send({success: false, error: err.message})
			} else {
				PoolSchema.findOne({online_users: max[0]["min"]}, function (err, all) {
					if (err || !all){
						res.status(512).send({success: false, error: err.message})
					} else {
						res.status(200).send({success: true, data: all})
					}
				})
			}
		})
	});

	app.get("/avg/dateofyear", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$dayOfYear: "$utc_time"},  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/max/dateofyear", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$dayOfYear: "$utc_time"},  max:{$max: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/max/year", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$year: "$utc_time"},  max:{$max: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/max/month", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$month: "$utc_time"},  max:{$max: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/min/month", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$month: "$utc_time"},  min:{$min: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});



	app.get("/avg/year", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$year: "$utc_time"},  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool})
			}
		})
	});

	app.get("/avg/dayofweek", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$dayOfWeek: "$utc_time"},  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool, msg: "Dim : 1, Sam : 7"})
			}
		})
	});

	app.get("/avg/isoweek", function(req,res){
		PoolSchema.aggregate([{$group: {_id: {$isoWeek: "$utc_time"},  avg:{$avg: "$online_users"}}}], function (err, pool) {
			if (err){
				res.status(512).send({success: false, error: err.message})
			} else {
				res.status(200).send({success: true, data: pool, msg: "No  de semaine"})
			}
		})
	});


	app.get("/", function (req,res) {
		res.status(200).send({success: true, msg: "Welcome to the api!", commit: process.env.COMMIT || "notfound" , method_doc: "https://www.getpostman.com/collections/c00923dd7caa278fe52a"})
	});

	app.use(function (req, res) {
		// Erreur 404
		res.status(404).send({success: false, err: 404})
	});

	app.listen(port, function () {
		console.log("Server started on : " + port)
	});



});
