/*
    datas
    chamillo_stats

    Created by Swano 27/02/19 13:06
*/
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PoolSchema = new Schema({
	online_users: Number,
	utc_time: Date
});

exports.PoolSchema = mongoose.model('PoolSchema', PoolSchema, 'online_users');
