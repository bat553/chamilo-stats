"""
Baptiste Pellarin 2019
baptiste.pellarin@gmail.com
"""
import datetime
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

mongdb_client = MongoClient('mongodb', 27017)
db = mongdb_client.chamilo
online_stats = db.online_users

def main():
    online_users = FetchHtml("https://chamilo.univ-grenoble-alpes.fr/courses/IUT1/?id_session=")
    online_users = int(online_users)
    print(online_users, "Users online")
    post = {
        "online_users": online_users,
        "utc_time": datetime.datetime.utcnow()
    }
    print(online_stats.insert_one(post).inserted_id)




def FetchHtml(url):
    response = requests.get(url)
    data = BeautifulSoup(response.text, "lxml")

    online_users = data.find("a", {"title": "Utilisateurs en ligne"}).text

    return (online_users)


if __name__ == "__main__":
    main()
